module BookKeeping
     VERSION = 3
end

class Hamming
  def self.compute(strand1, strand2)
    raise ArgumentError unless strand1.bytesize == strand2.bytesize 
    
    hamming_distance = 0

    unless strand1 == strand2      
      strand1.split('').each_with_index do |nucl_val, nucl_index|
        unless nucl_val == strand2.byteslice( nucl_index )
          hamming_distance += 1
        end
      end
    end 

    hamming_distance
  end
  rescue
end
